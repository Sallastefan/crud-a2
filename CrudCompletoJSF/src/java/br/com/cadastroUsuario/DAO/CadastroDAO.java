package br.com.cadastroUsuario.DAO;

import br.com.cadastro.entidade.CadastroUsuário;
import br.com.cadastro.util.FabricaConexao;
import com.mysql.jdbc.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CadastroDAO {//classe onde ficam os comandos de SQL

    public void inserir(CadastroUsuário mensagem) throws ClassNotFoundException, SQLException {
        try {
            Connection conexao = (Connection) FabricaConexao.getConexao();
            PreparedStatement pst;
            if (mensagem.getCodigo() == null) {
                pst = conexao.prepareCall("INSERT INTO mensagem (codigo,nome,email)"
                        + " values(null,?,?)");
            } else {
                pst = conexao.prepareCall("UPDATE mensagem set nome=?, email=? where codigo=?");
                pst.setInt(3, mensagem.getCodigo());
            }
            
            pst.setString(1, mensagem.getNome());
            pst.setString(2, mensagem.getEmail());
            pst.execute();
            FabricaConexao.fecharConexao();
        } catch (SQLException ex) {
            Logger.getLogger(CadastroDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<CadastroUsuário> selecionarTudo() throws ClassNotFoundException, SQLException {
        try {
            Connection conexao = (Connection) FabricaConexao.getConexao();
            PreparedStatement pst = conexao.prepareCall("SELECT * FROM mensagem");
            ResultSet rs = pst.executeQuery();
            List<CadastroUsuário> lista = new ArrayList<>();
            while (rs.next()) {
                CadastroUsuário msg = new CadastroUsuário();
                msg.setCodigo(rs.getInt("codigo"));
                msg.setNome(rs.getString("nome"));
                msg.setEmail(rs.getString("email"));
                lista.add(msg);
            }
            return lista;
        } catch (SQLException ex) {
            Logger.getLogger(CadastroDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    
  /* public void deletar(Mensagem mensagem) throws  ClassNotFoundException,SQLException{
     try{
         Connection connexao=(Connection) FabricaConexao.getConexao();
         PreparedStatement pst;
         
         if(mensagem.getCodigo()!=null){
         pst=connexao.prepareCall("DELETE FROM mensagem WHERE codigo=?;");
         pst.setInt(1,mensagem.getCodigo());
         pst.execute();
     }
         FabricaConexao.fecharConexao();
     }catch(SQLException ex){
         Logger.getLogger(MensagemDAO.class.getName()).log(Level.SEVERE,null,ex);
     }
     
   } */ 

    public void deletar(CadastroUsuário mensagem) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
