package br.com.cadastro.bean;

import br.com.cadastroUsuario.DAO.CadastroDAO;
import br.com.cadastro.entidade.CadastroUsuário;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean
@SessionScoped
public class CadastroBean {

    private CadastroUsuário mensagem = new CadastroUsuário();//objeto que herda os metodos get e set da classe Mensagem
    private CadastroDAO msg_dao = new CadastroDAO();//objeto que herda os metodos inserir deletar e listar e excluir da 
    //da classe MensagemDAO

    private List<CadastroUsuário> listamsg = new ArrayList<>();

    public void cadastrar() throws ClassNotFoundException, SQLException {
        // getMsg_dao().inserir(getMensagem());//executando o metodo inserir da classe DAO
        // setMensagem(new Mensagem());//passando o objeto mensagem para limpar a memoria
        new CadastroDAO().inserir(mensagem);
        mensagem = new CadastroUsuário();
    }

    public void listar() throws ClassNotFoundException, SQLException {
        listamsg = msg_dao.selecionarTudo();
    }
    
    public void alterar(CadastroUsuário m){
        mensagem = m;
    }
 
    //deletar bean
    public void deletar(CadastroUsuário mensagem) throws ClassNotFoundException,SQLException{
        new CadastroDAO().deletar(mensagem);
        listar(); 
    }
    
    
    public CadastroUsuário getMensagem() {
        return mensagem;
    }

    public void setMensagem(CadastroUsuário mensagem) {
        this.mensagem = mensagem;
    }

    public CadastroDAO getMsg_dao() {
        return msg_dao;
    }

    public void setMsg_dao(CadastroDAO msg_dao) {
        this.msg_dao = msg_dao;
    }

    public List<CadastroUsuário> getListamsg() {
        return listamsg;
    }

    public void setListamsg(List<CadastroUsuário> listamsg) {
        this.listamsg = listamsg;
    }

}
